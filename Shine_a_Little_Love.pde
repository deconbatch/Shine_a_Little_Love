/**
 * Shine a Little Love.
 * Tried various formulas and chose one.
 * ref. https://twitter.com/ntsutae/status/1348865018452791296
 *
 * @author @deconbatch
 * @version 0.1
 * @license GPL Version 3 http://www.gnu.org/licenses/
 * Processing 3.5.3
 * 2021.01.13
 */

void setup() {
  size(720, 720);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  rectMode(CENTER);
  smooth();
  noLoop();
}

void draw() {
  int   frmRate  = 24;
  int   frmCycle = frmRate * 3;       // morphing duration frames
  int   cycles   = 5;                 // animation cycle no
  int   frmMax   = frmCycle * cycles; // whole frames
  int   hW       = floor(width * 0.5);
  int   hH       = floor(height * 0.5);
  float hueBase  = random(360.0);

  translate(hW, hH);
  for(int cycle = 0; cycle < cycles; cycle++) {
    int div = floor(random(5, 10)) * 2;
    // ommit unsymmetrical 14
    if (div == 14) {
      div = 16;
    }
    float xRatio = random(0.02, 0.1);
    float yRatio = xRatio;
    hueBase += 360.0 / cycles;

    for(int frmCnt = 0; frmCnt < frmCycle; frmCnt++) {
      float frmRatio = map(frmCnt, 0, frmCycle, 0.0, 1.0);
      float easing = easeInOutCubic(frmRatio);

      // animate parameters
      float animX = 1.0 + frmRatio * 0.05 * div / 20.0;
      float animY = 0.01 + easing * 0.01 * div / 20.0;
    
      background(0.0, 0.0, 90.0, 100.0);
      stroke(0.0, 0.0, 90.0, 100.0);
      strokeWeight(1.0);
      for (int x = -hW; x < hW; x += div) {
        for (int y = -hH; y < hH; y += div) {
          float x0 = funcX(x * xRatio, y * yRatio, animX);
          float y0 = funcY(x * xRatio, y * yRatio, animY);
          float x1 = funcX(x0, y0, animX);
          float y1 = funcY(x0, y0, animY);
          float x2 = funcX(x1, y1, animX);
          float y2 = funcY(x1, y1, animY);

          float rHue = abs(hueBase + 360.0 + (x1 + y1) * 120.0) % 360.0;
          if (x2 * y2 >= 0) {
            fill(rHue, 60.0, 70.0, 100.0);
            rect(x, y, div, div);
          }
        }
      }  
      casing();
      saveFrame("frames/" + String.format("%04d", cycle) + ".01." + String.format("%04d", frmCnt) + ".png");
    }
    // for stop motion frames
    for (int i = 0; i < frmRate; i++) {
      saveFrame("frames/" + String.format("%04d", cycle) + ".02." + String.format("%04d", i) + ".png");
    }
  }
  exit();
}

/**
 * funcX : shape calculate function.
 */
private float funcX(float _a, float _b, float _c) {
  return sin(_a * _a - _b * _b) * cos(_a * _b * 2.0 * _c);
}

/**
 * funcY : shape calculate function.
 */
private float funcY(float _a, float _b, float _c) {
  return log(width + _a + pow(sin(_b), 2)) * _c;
}

/**
 * easeInOutCubic : easing function.
 */
private float easeInOutCubic(float _t) {
  _t *= 2.0;
  if (_t < 1.0) {
    return pow(_t, 3) / 2.0;
  }
  _t -= 2.0;
  return (pow(_t, 3) + 2.0) / 2.0;
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 50.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
  noStroke();
}
